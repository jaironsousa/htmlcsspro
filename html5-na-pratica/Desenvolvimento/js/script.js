$(document).ready(function() {
    // MENU
    $('#nav-toggle').click(function(e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $('.header-collapse').toggleClass('active');
    })

    // CARROSEL
    $('.owl-carousel').owlCarousel({
        items: 1,
        lazyLoad: true,
        loop: true,
        margin: 10
    });
});